package services

import common.With
import domain.SalesOrder
import org.scalatest.{FlatSpec, Matchers}

import scala.io.Source

class CachedDatasetServiceTest extends FlatSpec with Matchers{

  "getMostRecentOrdersByCustomerID" should "parse and cache all unique customer IDs without error" in {

    val underTest = new CachedDatasetService()

    val uniqueIds = With(Source.fromInputStream(this.getClass.getClassLoader.getResourceAsStream("SalesDataset.csv"))) { file =>
      val iterator = file.getLines
      val columnNames = iterator.next().split(";")

      (for {
        batch <- iterator.grouped(500)
        line <- batch
        splitLine = line.split(";")
      } yield splitLine(columnNames.indexOf("CustomerID"))).toArray.distinct
    }.getOrElse(new Array[String](0))

    val parsedOrders = new Array[Either[Throwable, List[SalesOrder]]](uniqueIds.length)

    var i = 0
    while (i < uniqueIds.length) {
      println(f"parsing order $i%d")
      parsedOrders(i) = underTest.getMostRecentOrdersByCustomerID(uniqueIds(i), 3)
      i += 1
    }

    val errors = parsedOrders.count(_.isLeft)

    errors should equal (0)
  }


}
