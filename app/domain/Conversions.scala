package domain

import java.time.{LocalDate, LocalDateTime}
import java.time.format.{DateTimeFormatter, DateTimeParseException}

import scala.util.Try

object Conversions {

  implicit def toLocalDateTime(date: String) : LocalDateTime = {
    Try(LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))).recoverWith{
      case _: DateTimeParseException => Try(
        LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy")).atStartOfDay()
      )
    }.get
  }

  def priceWithCommaToLong(number: String) : Long = {
    number.replaceAll(",", "").toLong
  }

}
