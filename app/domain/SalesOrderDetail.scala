package domain

case class SalesOrderDetail(salesOrderDetailID: Long, productID: String, orderQty: Int, unitPrice: Long, lineTotal: Long)

case object SalesOrderDetail {

  def default = SalesOrderDetail(0, "", 0, 0, 0)

}