package domain

import java.time.LocalDateTime

case class SalesOrder(salesOrderID: Long, orderDate: LocalDateTime, orderDetails: List[SalesOrderDetail]) extends Ordered[SalesOrder] {

  override def compare(that: SalesOrder): Int = {
    that.orderDate.compareTo(this.orderDate)
  }
}
