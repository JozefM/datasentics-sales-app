package controllers

import java.time.LocalDateTime

import javax.inject._
import play.api.mvc.InjectedController
import services.DatasetService

@Singleton
class DatasetController @Inject()(datasetservice: DatasetService) extends InjectedController {

  def info() = Action {
    datasetservice.countDatasetRows() match {
      case Right(size) => composeInfoReponse(size)
      case Left(err) => InternalServerError(err.getMessage)
    }
  }

  def getThreeOrders(id: String) = Action {
    datasetservice.getMostRecentOrdersByCustomerID(id, 3) match {
      case Right(orders) => Ok(views.html.orders(orders))
      case Left(err) => {
        InternalServerError("There was an error processing your request: " + err.getCause + " " + err.getMessage)
      }
    }
  }

  private def composeInfoReponse(size: Long) = {
    val sb = new StringBuffer()
    sb.append("Current time: ")
    sb.append(LocalDateTime.now())
    sb.append(", dateset row count is: ")
    sb.append(size)

    Ok(sb.toString)
  }
}
