package services

import common.With
import domain.{SalesOrder, SalesOrderDetail}

import scala.io.Source
import scala.util.Try

class CachedDatasetService extends DatasetService {

  import domain.Conversions._

  var customerIDIndex: Int = _
  var salesOrderIDIndex: Int = _
  var orderDateIndex: Int = _
  var salesOrderDetailIdIndex: Int = _
  var productIDIndex: Int = _
  var orderQtyIndex: Int = _
  var unitPriceIndex: Int = _
  var lineTotalIndex: Int = _

  val cache = collection.mutable.Map[String, List[SalesOrder]]()

  val rawLines = With(Source.fromInputStream(getFileInputStream("SalesDataset.csv"))) { source =>

    val lines = source.getLines.toList

    val columnNames = lines.head.split(";")
    customerIDIndex = columnNames.indexOf("CustomerID")
    salesOrderIDIndex = columnNames.indexOf("SalesOrderID")
    orderDateIndex = columnNames.indexOf("OrderDate")
    salesOrderDetailIdIndex = columnNames.indexOf("SalesOrderDetailID")
    productIDIndex = columnNames.indexOf("ProductID")
    orderQtyIndex = columnNames.indexOf("OrderQty")
    unitPriceIndex = columnNames.indexOf("UnitPrice")
    lineTotalIndex = columnNames.indexOf("LineTotal")

    lines.tail
  }

  override lazy val countDatasetRows: Either[Throwable, Long] = rawLines.map(_.size)

  override def getMostRecentOrdersByCustomerID(id: String, numberOfOrders: Int): Either[Throwable, List[SalesOrder]] =
    cache.get(id)
      .filter(_.size >= numberOfOrders)
      .map(orders => Right(orders.take(numberOfOrders)))
      .getOrElse(processMostRecentOrdersByCustomerID(id, numberOfOrders))

  private def processMostRecentOrdersByCustomerID(id: String, numberOfOrders: Int): Either[Throwable, List[SalesOrder]] = {
    rawLines.map(lines => {

      val sortedOrders = iterateLinesInGroups(lines.iterator, id)
        .groupBy(row => (row(salesOrderIDIndex), row(orderDateIndex)))
        .mapValues(_.map(arr => createOrderDetailWithDefault(arr)))
        .map { case ((orderId, date), details) => SalesOrder(orderId.toLong, date, details) }
        .toList
        .sorted

      cache(id) = sortedOrders
      sortedOrders.take(numberOfOrders)
    })
  }


  def createOrderDetailWithDefault(arr: Array[String]) = {
    Try(
      SalesOrderDetail(
        arr(salesOrderDetailIdIndex).toInt,
        arr(productIDIndex),
        arr(orderQtyIndex).toInt,
        priceWithCommaToLong(arr(unitPriceIndex)),
        priceWithCommaToLong(arr(lineTotalIndex)))
    ).recover { case _ => SalesOrderDetail.default }.get
  }

  def iterateLinesInGroups(iterator: Iterator[String], id: String) = {
    val groupedIterator = iterator.grouped(500)

    (for {
      batch <- groupedIterator
      line <- batch
      splitLine = line.split(";")
      if splitLine(customerIDIndex) == id
    } yield splitLine).toList
  }

  private def getFileInputStream(filename: String) = this.getClass.getClassLoader.getResourceAsStream(filename)
}