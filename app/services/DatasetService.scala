package services

import domain.SalesOrder

trait DatasetService {

  def countDatasetRows(): Either[Throwable, Long]
  def getMostRecentOrdersByCustomerID(id: String, numberOfOrders: Int): Either[Throwable, List[SalesOrder]]

}
