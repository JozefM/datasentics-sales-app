package common

import scala.util.Try

object With {

  def apply[A <: AutoCloseable, B](resource: => A)(run: A => B): Either[Throwable, B] ={
    val tryResource = Try(run(resource))
    resource.close()
    tryResource.toEither
  }

}